package com.projects.springboot.service;

import com.projects.springboot.dao.ILabelDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LabelService implements ILabelService {

    @Autowired
    private ILabelDao labelDao;

    /**
     * get Label for parameter, check if the name of label exists in DB
     * if it exists, just add map between label and task.
     * else add label at labeltable and map.
     *
     * @param label
     * @param taskId
     * @return whether success or not
     */
    @Override
    public boolean addLabel(String label, int taskId) {
        int labelId = labelDao.existsLabelName(label);
        if (labelId == 0) {//not exists
            labelId = labelDao.addLabelAtLabeltable(label);
            if (labelId > 0) {
                return labelDao.addMapBetweenLabelAndTask(taskId, labelId) > 0;
            }
        } else {
            return labelDao.addMapBetweenLabelAndTask(labelId, taskId) > 0;
        }
        return false;
    }

    @Override
    public boolean deleteLabel(int taskId, int labelId) {
        return false;
    }

    /**
     * delete all label_map rows by taskid
     *
     * @param taskId
     * @return if success or not
     */
    @Override
    public boolean deleteLabelByTaskId(int taskId) {
        return labelDao.deleteLabelByTaskId(taskId) > 0;
    }


}
