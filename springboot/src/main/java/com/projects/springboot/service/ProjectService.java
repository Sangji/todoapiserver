package com.projects.springboot.service;

import com.projects.springboot.dao.IProjectDao;
import com.projects.springboot.dao.ITaskDao;
import com.projects.springboot.model.Project;
import com.projects.springboot.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectService implements IProjectService {
    @Autowired
    private IProjectDao projectDao;

    @Autowired
    private ITaskDao taskDao;

    /**
     * call projectsdao to insert a project from Controller
     * if given project has not parent project, just insert.
     * else if given project has parent project, its order, level changes depend on parent
     *
     * @param project
     * @param parentId
     * @return
     */
    @Override
    public int createProject(Project project, int parentId) {
        int projectId = 0;
        if (parentId == 0) {//no parent
            projectId = projectDao.insertProject(project);
        } else {

            //select parent's info for getting order,level,id
            Project parentProject = projectDao.selectProjectById(parentId);

            //update orders
            projectDao.updateOrders(parentProject.getGroupId(), parentProject.getSeq());

            //set project's groupId, order , level depends on parent project
            project.setGroupId(parentProject.getGroupId());
            project.setSeq(parentProject.getSeq() + 1);
            project.setLevel(parentProject.getLevel() + 1);
            projectId = projectDao.insertSubProject(project);//insert project
        }
        return projectId;
    }

    /**
     * call projectsdao to delete project by Id from project table
     *
     * @param projectId
     * @return
     */
    @Override
    public boolean deleteProject(int projectId) {
        return projectDao.deleteProject(projectId);
    }

    /**
     * call projectdao to get all list of projects
     *
     * @return list of projects
     */
    @Override
    public List<Project> getAllProjects() {
        return projectDao.selectAll();
    }

    @Override
    public int updateProject(Project project) {
        return projectDao.updateProject(project);
    }


    public List<Task> getTasksByProjectId(int ProjectId) {
        Project project = projectDao.selectProjectById(ProjectId);
        return taskDao.getTaskByProject(project);
    }

}
