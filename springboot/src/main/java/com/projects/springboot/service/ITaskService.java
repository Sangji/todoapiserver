package com.projects.springboot.service;

import com.projects.springboot.model.Task;

import java.util.List;

public interface ITaskService {
    int addTask(int projectId, Task task, List<String> labelList);

    Task getTaskById(int id);

    List<Task> getAllTaskAsList();

    boolean updateTaskById(int id, Task task, List<String> labelList);

    boolean deleteTaskById(int id);

    List<Task> searchTaskByProjectName(String[] labelNameList);
}
