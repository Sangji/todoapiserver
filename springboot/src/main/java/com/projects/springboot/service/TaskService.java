package com.projects.springboot.service;

import com.projects.springboot.dao.ITaskDao;
import com.projects.springboot.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TaskService implements ITaskService {

    @Autowired
    private ITaskDao taskDAO;

    @Autowired
    private LabelService labelService;

    /**
     * add taskinfo which is given by user
     * if succeeded adding task, map taskId and projectId, then add labels of the task
     *
     * @param projectId
     * @param task
     * @return the generated id of task
     */
    public synchronized int addTask(int projectId, Task task, List<String> labelList) {
        int taskid = taskDAO.addTask(task);
        if (taskid > 0) {//succeeded add task
            taskDAO.MapTaskToProject(projectId, taskid);
            for (String aLabelList : labelList) {
                labelService.addLabel(aLabelList, taskid);
            }
        }
        return taskid;
    }

    /**
     * get task using id
     *
     * @param id
     * @return the task where id == given id
     */
    @Override
    public Task getTaskById(int id) {
        return taskDAO.getTaskbyId(id);
    }

    /**
     * get all tasks in database
     *
     * @return list of tasks
     */
    @Override
    public List<Task> getAllTaskAsList() {
        return taskDAO.getAllTask();
    }

    /**
     * update task using id with task which is changed
     *
     * @param taskId
     * @param task
     * @param labelList
     * @return if success or not
     */
    @Override
    public synchronized boolean updateTaskById(int taskId, Task task, List<String> labelList) {
        int count = taskDAO.updateTask(taskId, task);
        if (count > 0) {
            labelService.deleteLabelByTaskId(taskId);//전체 삭제
            for (String aLabelList : labelList) {
                labelService.addLabel(aLabelList, taskId);//전체 추가
            }
        }
        return count > 0;
    }

    /**
     * delete task using id
     * delete labelmap by taskid
     *
     * @param taskId task id
     * @return if success or not
     */
    @Override
    public boolean deleteTaskById(int taskId) {
        labelService.deleteLabelByTaskId(taskId);
        return taskDAO.deleteTask(taskId) > 0;
    }

    @Override
    public List<Task> searchTaskByProjectName(String[] labelNameList) {
        return null;
    }

    /**
     * get label name list as parameter
     *
     * @param labelNameList
     * @return
     */
    public List<Task> searchTaskByLabelNameList(String[] labelNameList) {
        return taskDAO.getTaskByLabelNameList(labelNameList);
    }


}
