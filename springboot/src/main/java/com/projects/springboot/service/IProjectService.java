package com.projects.springboot.service;

import com.projects.springboot.model.Project;

import java.util.List;

public interface IProjectService {
    // 프로젝틀를 생성하고 제거한다.
    // project의 수정된 정보들을 받아 각 Db에 오버라이딩
    // project 다 불러오기


    // project 검색
    int createProject(Project project, int parentId);

    boolean deleteProject(int projectId);

    List<Project> getAllProjects();

    int updateProject(Project project);


}
