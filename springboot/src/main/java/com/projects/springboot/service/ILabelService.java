package com.projects.springboot.service;

public interface ILabelService {
    //label을 추가/삭제하는 비지니스 로직을 구현한다.
    //label로 task를 검색하는 비지니스 로직을 구현한다.  => taskservice

    boolean addLabel(String label, int taskId);

    boolean deleteLabel(int taskId, int labelId);

    boolean deleteLabelByTaskId(int taskId);

}
