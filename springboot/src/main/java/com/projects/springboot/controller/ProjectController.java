package com.projects.springboot.controller;


import com.projects.springboot.model.Project;
import com.projects.springboot.model.Task;
import com.projects.springboot.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    /**
     * get name, parent project Id, level from Client and pass the project bean to ProjectService
     * ADD PROJECT
     *
     * @param project
     * @return
     */
    @RequestMapping(value = "/project", method = RequestMethod.POST)
    public ResponseEntity<?> post(Project project, int parentId) {
        int generatedId = projectService.createProject(project, parentId);
        if (generatedId > 0) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @RequestMapping(value = "/project/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@PathVariable("id") int id, Project project) {
        int count = projectService.updateProject(project);
        if (count > 0) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public ResponseEntity<List<Project>> getAll() {
        List<Project> projectList = projectService.getAllProjects();
        if (projectList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(projectList, HttpStatus.OK);
    }

    @RequestMapping(value = "/project/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") int id) {
        return new ResponseEntity<>(projectService.deleteProject(id) ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
    }


    /**
     * get Tasks by project Id. It contains subproject's tasks of the given projectid
     *
     * @param ProjectId
     * @return list of Tasks
     */
    @RequestMapping(value = "project/{id}/tasks", method = RequestMethod.GET)
    public ResponseEntity<List<Task>> getByProjectId(@PathVariable("id") int ProjectId) {
        List<Task> taskList = projectService.getTasksByProjectId(ProjectId);
        if (taskList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(taskList, HttpStatus.OK);
    }


}
