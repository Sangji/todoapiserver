package com.projects.springboot.controller;

import com.projects.springboot.model.Task;

import com.projects.springboot.service.LabelService;
import com.projects.springboot.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Arrays;
import java.util.List;


@RestController
public class TaskController {
    //TODO : Task 검색조건이 다양함. project 이름으로도 검색/label로도 검색 이 모든 처리는 Task Controller에서?
    @Autowired
    private TaskService taskService;

    @Autowired
    private LabelService labelService;

    //Add a Task
    @RequestMapping(value = "/task", method = RequestMethod.POST)
    public ResponseEntity<?> post(int projectId, Task task, String[] labelList) {
        int generatedId = taskService.addTask(projectId, task, Arrays.asList(labelList));
        if (generatedId > 0) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    //Get All Tasks
    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public ResponseEntity<List<Task>> get() {
        List<Task> taskList = taskService.getAllTaskAsList();
        if (taskList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<Task>>(taskList, HttpStatus.OK);
    }

    //Get Task By Id
    @RequestMapping(value = "/task/{id}", method = RequestMethod.GET)
    public ResponseEntity<Task> get(@PathVariable("id") int id) {
        Task task = taskService.getTaskById(id);
        if (task == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    //Update Task By ID
    @RequestMapping(value = "/task/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Task> put(@PathVariable("id") int id, Task task, String[] labelList) {
        boolean succeeded = taskService.updateTaskById(id, task, Arrays.asList(labelList));
        return new ResponseEntity<>(succeeded ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
    }

    //Delete Task By ID
    @RequestMapping(value = "/task/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") int id) {
        return new ResponseEntity<Void>(taskService.deleteTaskById(id) ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
    }


    /**
     * Get LabelNameList for searching by parameter
     * search tasks with constains the labelname of pamameter
     *
     * @param filter
     * @param labelNameList
     * @return TaskList which contains Tasks satisfying the condition . the condition is whether a task's label contains any labelname of labelnamelist.
     */
    @RequestMapping(value = "/tasks/search", method = RequestMethod.GET)
    public ResponseEntity<List<Task>> search(String filter, String[] labelNameList) {
        List<Task> taskList = null;
        if (filter.equals("label")) {
            taskList = taskService.searchTaskByLabelNameList(labelNameList);
        }
        return new ResponseEntity<>(taskList, taskList != null ? HttpStatus.OK : HttpStatus.NO_CONTENT);
    }


}
