package com.projects.springboot.mapper;

import com.projects.springboot.model.Task;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TaskMapper implements RowMapper<Task> { //DB 결과를 row로 mapping

    @Override
    public Task mapRow(ResultSet row, int rownum) throws SQLException {
        Task task = new Task();
        task.setId(row.getInt("id"));
        task.setTitle(row.getString("title"));
        task.setDetail(row.getString("detail"));
        task.setDone(row.getBoolean("is_done"));
        return task;
    }
}
