package com.projects.springboot.mapper;

import com.projects.springboot.model.Label;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class LabelMapper implements RowMapper<Label> {

    @Override
    public Label mapRow(ResultSet rs, int rowNum) throws SQLException {
        Label label = new Label();
        label.setId(rs.getInt("id"));
        label.setName(rs.getString("name"));
        return label;
    }
}
