package com.projects.springboot.mapper;

import com.projects.springboot.model.Project;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ProjectMapper implements RowMapper<Project> {


    @Override
    public Project mapRow(ResultSet rs, int rowNum) throws SQLException {
        Project project = new Project();
        project.setId(rs.getInt("id"));
        project.setName(rs.getString("name"));
        project.setGroupId(rs.getInt("group_id"));
        project.setSeq(rs.getInt("seq"));
        project.setLevel(rs.getInt("level"));
        return project;
    }
}
