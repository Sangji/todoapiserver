package com.projects.springboot;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;



import java.util.Arrays;

@SpringBootApplication
public class SpringbootApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(SpringbootApplication.class, args);

		System.out.println("Let's inspect the beans provided by Spring Boot:");

		String[] beanNames = ctx.getBeanDefinitionNames();
		Arrays.sort(beanNames);
		for (String beanName : beanNames) {
			System.out.println(beanName);
		}
	}

	/**Sql sessionfactory에 datasource 설정
	 * mybatis의
	 *


	@Bean //Spring에 필요한 객체를 생성할 때 사용
	public SqlSessionFactory sqlSessionFactory(DataSource ds)throws Exception{
		SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
		sessionFactoryBean.setDataSource(ds);

		//xml mapper 인식
		Resource[] res =new PathMatchingResourcePatternResolver().getResources("classpath:mappers/*.xml");
		sessionFactoryBean.setMapperLocations(res);

		return sessionFactoryBean.getObject();
	}*/
}


