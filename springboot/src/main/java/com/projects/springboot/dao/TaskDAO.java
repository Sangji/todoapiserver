package com.projects.springboot.dao;

import com.projects.springboot.mapper.TaskMapper;
import com.projects.springboot.model.Project;
import com.projects.springboot.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.Arrays;
import java.util.List;

@Transactional
@Repository
public class TaskDao implements ITaskDao {//DB access object
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public List<Task> getAllTask() {
        String sql = "select * from task";
        RowMapper<Task> taskRowMapper = new TaskMapper();
        return this.jdbcTemplate.query(sql, taskRowMapper);
    }

    //Add Task
    @Override
    public int addTask(Task task) {
        KeyHolder key = new GeneratedKeyHolder();
        String sql = "insert into task (title,detail) values (?,?)";
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, task.getTitle());
                ps.setString(2, task.getDetail());
                return ps;
            }
        }, key);
        return key.getKey().intValue();
    }

    @Override
    public int MapTaskToProject(int projectId, int taskId) {
        String sql = "insert into project_map(`project_id`,`task_id`) values(?,?)";
        return jdbcTemplate.update(sql, projectId, taskId);
    }


    //GET ALL TASK
    @Override
    public Task getTaskbyId(int id) {
        String sql = "select * from task where id=?";
        RowMapper<Task> taskRowMapper = new BeanPropertyRowMapper<Task>(Task.class);//
        return jdbcTemplate.queryForObject(sql, taskRowMapper, id);
    }

    //UPDATE Task By ID
    @Override
    public int updateTask(int id, Task task) {
        String sql = "update task set title=? , detail=? where id=?";
        return jdbcTemplate.update(sql, task.getTitle(), task.getDetail(), id);
    }

    //DELETE Task BY ID
    @Override
    public int deleteTask(int id) {
        String sql = "delete from task where id=?";
        return jdbcTemplate.update(sql, id);
    }

    /**
     * get labelnamelist from Service as a paramter
     * get tasks contais any labelname in the list
     *
     * @param labelNameList
     * @return tasks as a List
     */
    @Override
    public List<Task> getTaskByLabelNameList(String[] labelNameList) {
        String sql = "select t.* from label_map lm, task t, label l where lm.label_id=l.id and (l.name in (:labelNames)) and t.id=lm.task_id group by t.id";
        RowMapper<Task> taskRowMapper = new BeanPropertyRowMapper<Task>(Task.class);


        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("labelNames", Arrays.asList(labelNameList));
        return namedParameterJdbcTemplate.query(sql, parameterSource, taskRowMapper);

    }

    /**
     * 해당 project 와 같은 그룹의 하위 프로젝트에 있는 Task들을 select한다.
     * select tasks where in subprojects of given project
     *
     * @param project
     * @return taskList or null
     */
    @Override
    public List<Task> getTaskByProject(Project project) {
        String sql = "select t.* from task t, project p, project_map pm where pm.task_id=t.id and p.id=pm.project_id and p.group_id=? and p.level>?";
        RowMapper<Task> taskRowMapper = new BeanPropertyRowMapper<Task>(Task.class);
        return jdbcTemplate.query(sql, taskRowMapper, project.getGroupId(), project.getLevel());
    }


}
