package com.projects.springboot.dao;

public interface ILabelDao {
    int addLabelAtLabeltable(String label);

    int addMapBetweenLabelAndTask(int taskId, int labelId);

    int existsLabelName(String labelName);

    boolean deleteLabelAtLabeltable(int labelId);

    int deleteLabelByTaskId(int taskId);

    boolean deleteMapBetweenLabelAndTask(int taskId, int labelId);
}
