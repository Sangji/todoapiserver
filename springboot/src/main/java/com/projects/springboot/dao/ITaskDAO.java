package com.projects.springboot.dao;

import com.projects.springboot.model.Project;
import com.projects.springboot.model.Task;

import java.util.List;

public interface ITaskDao {

    List<Task> getAllTask();

    int addTask(Task task);//Create

    int MapTaskToProject(int projectId, int taskId);

    Task getTaskbyId(int id);//Read

    int updateTask(int id, Task task);//Update

    int deleteTask(int id);//Delete

    List<Task> getTaskByLabelNameList(String[] labelNameList);

    List<Task> getTaskByProject(Project project);




}
