package com.projects.springboot.dao;

import com.projects.springboot.mapper.ProjectMapper;
import com.projects.springboot.model.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Transactional
@Repository
public class ProjectDao implements IProjectDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    /**
     * insert project to Mysql
     *
     * @param project
     * @return projectId if it succeeded,
     * //todo: should check what does key return if failed to insert
     */
    @Override
    public int insertProject(Project project) {
        KeyHolder key = new GeneratedKeyHolder();
        String sql = "insert into project (`name`,`group_id`,`seq`,`level`) values (?,(select last_insert_id()+1),1,0)";
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, project.getName());
                return ps;
            }
        }, key);

        return key.getKey().intValue();
    }

    @Override
    public int insertSubProject(Project project) {

        KeyHolder key = new GeneratedKeyHolder();
        String sql = "insert into project (`name`,`group_id`,`seq`,`level`) values (?,?,?,?)";
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, project.getName());
                ps.setInt(2, project.getGroupId());
                ps.setInt(3, project.getSeq());
                ps.setInt(4, project.getLevel());
                return ps;
            }
        }, key);

        return key.getKey().intValue();
    }

    /**
     * delete project by projectid
     *
     * @param projectId
     * @return if it succeeded or failed
     */
    @Override
    public boolean deleteProject(int projectId) {
        String sql = "delete from project where id = ?";
        return jdbcTemplate.update(sql, projectId) > 0;
    }

    /**
     * update project
     *
     * @param project
     * @return
     */
    @Override
    public int updateProject(Project project) {
        String sql = "update  project set name=? , group_id=?, seq=?, level=?, task_id=? where id =?";
        return jdbcTemplate.update(sql, project.getName(), project.getGroupId(), project.getSeq(), project.getLevel(), project.getTaskId(), project.getId()
        );
    }

    @Override
    public int updateOrders(int groupId, int parentorder) {
        String sql = "update  project set seq=seq+1 where group_id=? AND seq>?";
        return jdbcTemplate.update(sql, groupId, parentorder);
    }

    /**
     * get All Projects from table
     *
     * @return the list of Projects
     */
    @Override
    public List<Project> selectAll() {
        String sql = "select * from project";
        RowMapper<Project> projectRowMapper = new ProjectMapper();
        return jdbcTemplate.query(sql, projectRowMapper);
    }

    @Override
    public Project selectProjectById(int id) {
        String sql = "select * from project where id =?";
        RowMapper<Project> projectRowMapper = new BeanPropertyRowMapper<>(Project.class);
        return jdbcTemplate.queryForObject(sql, projectRowMapper, id);
    }


}
