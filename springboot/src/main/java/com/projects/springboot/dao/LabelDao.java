package com.projects.springboot.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

@Transactional
@Repository
public class LabelDao implements ILabelDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * add label at label table
     *
     * @param label
     * @return labelid if succeeded
     */
    @Override
    public int addLabelAtLabeltable(String label) {
        KeyHolder key = new GeneratedKeyHolder();
        String sql = "insert into label (name) values (?)";
        int affectedRows = jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, label);
                return ps;
            }
        }, key);

        if (affectedRows > 0) {        //if the number of affected rows is more than 0,it means succeeded
            return key.getKey().intValue();
        }
        return affectedRows;//affectedRows==0 : failed
    }


    /**
     * add labelid and task id  at labelmap
     *
     * @return mapid if succeeded
     */
    @Override
    public int addMapBetweenLabelAndTask(int taskId, int labelId) {
        String sql = "insert into label_map (task_id,label_id) values (?,?)";
        return jdbcTemplate.update(sql, taskId, labelId);
    }

    /**
     * check if labelname exists
     *
     * @return if succeeded or not
     */
    @Override
    public int existsLabelName(String labelName) {
        String sql = "select id from label where name='" + labelName + "'";
        int labelId;
        try {
            labelId = jdbcTemplate.queryForObject(sql, Integer.class);
        } catch (EmptyResultDataAccessException e) {// if not exists
            labelId = 0;
        }
        return labelId;
    }


    /**
     * delete label name at label table
     *
     * @return if succeed or not
     */
    @Override
    public boolean deleteLabelAtLabeltable(int labelId) {

        return false;
    }

    /**
     * delete label by taskId
     *
     * @param taskId
     * @return the number of affected rows
     */
    @Override
    public int deleteLabelByTaskId(int taskId) {
        String sql = "delete from label_map where task_id=?";
        return jdbcTemplate.update(sql, taskId);
    }

    /**
     * delete mapping at labelmap
     *
     * @return if succeed or not
     */
    @Override
    public boolean deleteMapBetweenLabelAndTask(int taskId, int labelId) {
        return false;
    }


}
