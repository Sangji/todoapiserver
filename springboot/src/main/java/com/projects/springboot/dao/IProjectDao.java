package com.projects.springboot.dao;

import com.projects.springboot.model.Project;

import java.util.List;

public interface IProjectDao {

    int insertProject(Project project);

    int insertSubProject(Project project);

    boolean deleteProject(int projectId);

    int updateProject(Project project);

    int updateOrders(int groupId, int parentorder);

    List<Project> selectAll();

    Project selectProjectById(int id);

}
